package com.example.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datastorage.R;
import com.example.datastorage.adapter.MovieRecyclerAdapter;
import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.dao.MoviesDao;
import com.example.datastorage.data.ImageData;
import com.example.datastorage.entity.Movies;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MoviesActivity extends AppCompatActivity {

    private final static int READ_INTERNAL_STORAGE = 100;

    private List<Movies> movieDataSet;
    private MovieRecyclerAdapter movieAdapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView rcvMovie;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        rcvMovie = findViewById(R.id.rcv_movie);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(v -> {
            Intent addIntent = new Intent(this,AddMovieActivity.class);
            startActivity(addIntent);
        });


        getMovieData();

        if(!checkPermission()){
            requestPermission();
        }

    }

    private void getMovieData(){
        // Get database here...
        movieDataSet = new ArrayList<>();
        movieDataSet = RoomDatabaseConfig.getRoomDB(this).moviesDao().selectAll();
        Log.d("yes",movieDataSet.toString());
        layoutManager = new LinearLayoutManager(this);
        movieAdapter = new MovieRecyclerAdapter(movieDataSet,this);
        rcvMovie.setLayoutManager(layoutManager);
        rcvMovie.setAdapter(movieAdapter);
        movieAdapter.notifyDataSetChanged();

    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,
               new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                       READ_INTERNAL_STORAGE);
    }

    private boolean checkPermission(){
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            return false;
        }
    }
}
