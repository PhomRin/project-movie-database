package com.example.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.datastorage.R;
import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.entity.Movies;

public class AddMovieActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnChooseImage;
    private Button btnAdd;
    private EditText editTitle;
    private EditText editDesc;
    private ImageView imagePreview;
    private Uri imageUri;
    private int updateId;

    private final static int CHOOSE_IMAGE_REQUEST_CODE = 1;

    private void initView(){
        btnChooseImage = findViewById(R.id.button_chooseImage);
        btnAdd = findViewById(R.id.button_add);
        editTitle = findViewById(R.id.edit_Title);
        editDesc = findViewById(R.id.edit_Description);
        imagePreview = findViewById(R.id.image_preview);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        initView();

        btnChooseImage.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        onNewIntent(getIntent());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_chooseImage:{
                //Choose image from gallery
                Intent imageIntent = new Intent(Intent.ACTION_PICK);
                imageIntent.setType("image/*");
                startActivityForResult(imageIntent, CHOOSE_IMAGE_REQUEST_CODE);
            }break;
            case R.id.button_add: {
                if (btnAdd.getText()=="update"){
                    String title = editTitle.getText().toString();
                    String desc = editDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movies movies = new Movies(title,desc,thumbnail);
                    movies.setId(updateId );
                    RoomDatabaseConfig.getRoomDB(this).moviesDao().updateOne(movies);
                    Log.i("TAG","Update Success");
                    finish();
                }else {
                    String title = editTitle.getText().toString();
                    String desc = editDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movies movies = new Movies(title,desc,thumbnail);
                    RoomDatabaseConfig.getRoomDB(this).moviesDao().insert(movies);
                    Log.i("TAG","Success");
                    finish();
                }

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_IMAGE_REQUEST_CODE
            && resultCode == RESULT_OK
            && data != null){
            imageUri = data.getData();

            imagePreview.setImageURI(imageUri);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null){
            if (extras.getBoolean("update")){
                Movies movies = (Movies)extras.getSerializable("editMovie");
                updateId = movies.getId();
                btnAdd.setText("update");
                editTitle.setText(movies.getTitle());
                editDesc.setText(movies.getDescription());
            }
        }
    }
}
