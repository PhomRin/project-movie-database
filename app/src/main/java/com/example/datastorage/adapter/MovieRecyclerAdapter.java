package com.example.datastorage.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.datastorage.R;
import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.entity.Movies;
import com.example.ui.AddMovieActivity;

import java.util.List;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieRecyclerAdapter.MovieViewHolder> {

        private List<Movies> movieDataSet;
        private Context context;

        public MovieRecyclerAdapter(List<Movies> movieDataSet,Context context){
            this.movieDataSet = movieDataSet;
            this.context = context;


        }



    @NonNull
    @Override
    public MovieRecyclerAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);
        context = parent.getContext();
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieRecyclerAdapter.MovieViewHolder holder, int position) {
        holder.textTitle.setText(movieDataSet.get(position).getTitle());
        holder.textDesc.setText(movieDataSet.get(position).getDescription());
        holder.imageMovie.setImageURI(movieDataSet.get(position).getThumbnail());

        }

    @Override
    public int getItemCount() {
        return movieDataSet.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder{
        ImageView imageMovie, imageMore;
        TextView textTitle, textDesc;
        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageMovie = itemView.findViewById(R.id.image_movie);
            textTitle = itemView.findViewById(R.id.txt_movie_title);
            textDesc = itemView.findViewById(R.id.txt_movie_desc);
            imageMore = itemView.findViewById(R.id.image_more);

            imageMore.setOnClickListener(v -> {
                PopupMenu menu = new PopupMenu(context,imageMore);
                menu.getMenuInflater().inflate(R.menu.option_menu, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_delete:{
                                RoomDatabaseConfig.getRoomDB(context).moviesDao().deleteOne(movieDataSet.get(getAdapterPosition()));
                            }
                            case R.id.item_edit:{
                                Movies movies =new Movies(
                                        movieDataSet.get(getAdapterPosition()).getTitle(),
                                        movieDataSet.get(getAdapterPosition()).getDescription(),
                                        null
                                        );

                                movies.setId(movieDataSet.get(getAdapterPosition()).getId());
                                Intent updateIntent = new Intent(context, AddMovieActivity.class);
                                updateIntent.putExtra("update", true);
                                updateIntent.putExtra("editMovie",movies);
                                context.startActivity(updateIntent);
                            }
                        }
                        return true;
                    }
                });
                menu.show();
            });

        }
    }
}
