package com.example.datastorage.config;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.datastorage.dao.MoviesDao;
import com.example.datastorage.dao.UserDao;
import com.example.datastorage.entity.Movies;
import com.example.datastorage.entity.User;

@Database(entities = {Movies.class, User.class}, version = 1)
@TypeConverters({UriConverter.class})

public abstract class RoomDatabaseConfig extends RoomDatabase{

    // Create dao object
    public abstract MoviesDao moviesDao();
    public abstract UserDao userDao();

    // Create singleton object
    public static RoomDatabaseConfig INSTANCE;

    // Initialize singleton object
    public static RoomDatabaseConfig getRoomDB(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
            RoomDatabaseConfig.class,"db_movies").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    // For destroy singleton object
    public static void destroy(){
        INSTANCE=null;
    }
}
