package com.example.datastorage.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;

import com.example.datastorage.R;
import com.example.datastorage.entity.User;

import java.util.List;

public class RoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        AppDatabase db = Room.databaseBuilder(this,
                AppDatabase.class,"ShortDB")
                .allowMainThreadQueries()
                .build();

        db.getUserDao().insert(new User("Rin","3112"));
        List<User> userList = db.getUserDao().findAll();
        Log.d("tag",userList.toString());
    }
}
