package com.example.datastorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private TextView textData;
    private Button btn_Get;
    String message = "I love you, Will you marry me?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        //Manipulate (add...)

        editor.putString("secret", "12345");
        editor.apply();

        textData = findViewById(R.id.text_mydata);
        btn_Get = findViewById(R.id.button_get);

        if (prefs.contains("secret")) {
            String secret = prefs.getString("secret", "No data");
            textData.setText(secret);
        } else {
            textData.setText("Data Removed");
        }


        btn_Get.setOnClickListener(v -> {

            read();
        });
        write();
    }

    void write(){

        try {

            OutputStreamWriter outputStream = new OutputStreamWriter(openFileOutput("internal", MODE_APPEND));
            outputStream.write(message);
            outputStream.close();

            Toast.makeText(this,"Yes",Toast.LENGTH_LONG).show();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    void read(){
        try {
            FileInputStream fileInputStream = openFileInput("internal");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String msg = bufferedReader.readLine();
            Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
