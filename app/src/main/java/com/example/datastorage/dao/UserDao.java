package com.example.datastorage.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.datastorage.entity.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("select * from tblUser")
    List<User> findAll();

    @Insert
    void insert(User... users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);

}
