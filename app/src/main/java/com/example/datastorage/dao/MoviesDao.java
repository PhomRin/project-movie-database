package com.example.datastorage.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.datastorage.entity.Movies;

import java.util.List;

        @Dao
            public interface MoviesDao {

        @Insert
        void insert(Movies... movies);

        @Query("SELECT * FROM movies")
        List<Movies> selectAll();

        @Query("SELECT * FROM movies WHERE id =:id")
        Movies selectOne(int id);

        @Update
        void updateOne(Movies movies);

        @Delete
        void deleteOne(Movies... movies);
        }
